# serde-deftly

This is an experiment
to see how much of `#[derive(serde)]`
I can reimplement using
[`derive-deftly`](https://docs.rs/derive-deftly/latest/derive_deftly/).

Don't use this for anything.
