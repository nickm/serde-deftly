use derive_deftly::define_derive_deftly;

define_derive_deftly! {
    // XXXX Is there a "struct or enum but not union" syntax?
pub Deserialize =

${defcond ENUM_UNTAGGED tmeta(serde(untagged)) }
${defcond ENUM_INTERNALLY_TAGGED all(tmeta(serde(tag)), not(tmeta(serde(content)))) }
${defcond ENUM_ADJACENTLY_TAGGED all(tmeta(serde(tag)), tmeta(serde(content))) }

${defcond ONE_FIELD approx_equal({${for fields {x}}}, {x}) }
${defcond NO_SKIPS approx_equal(${for fields { ${when F_SKIP} skip }}, {})}
${defcond V_IS_NEWTYPE all(v_is_tuple, ONE_FIELD, NO_SKIPS) }
${defcond AS_MAP v_is_named}
${define N_FIELDS {( ${for fields { ${when not(F_SKIP)} 1 + }} 0 )}}

${defcond V_SKIP any(vmeta(serde(skip)), vmeta(serde(skip_deserializing))) }
${defcond F_SKIP any(fmeta(serde(skip)), fmeta(serde(skip_deserializing))) }

${define F_DEFAULT {$crate::m::Default::default()} }

${define F_NAME_ATTR {
    ${if fmeta(serde(rename(deserialize))) {
        #[deftly(name = ${fmeta(serde(rename(deserialize))) as str} )]
    } else if fmeta(serde(rename)) {
        #[deftly(name = ${fmeta(serde(rename)) as str} )]
    }}
}}

${define V_NAME_ATTR {
    ${if vmeta(serde(rename(deserialize))) {
        #[deftly(name = ${vmeta(serde(rename(deserialize))) as str} )]
    } else if vmeta(serde(rename)) {
        #[deftly(name = ${vmeta(serde(rename)) as str} )]
    }}
}}

${define T_NAME {
    ${if tmeta(serde(rename(deserialize))) {
        ${tmeta(serde(rename(deserialize))) as str}
    } else if tmeta(serde(rename)) {
        ${tmeta(serde(rename)) as str}
    } else {
        $crate::m::stringify!($tname)
    }}
}}

${defcond HAS_VARIANT_TAG all(is_enum, not(ENUM_UNTAGGED)) }
${defcond HAS_TOPLEVEL_VISITOR not(ENUM_UNTAGGED) }

${if all(ENUM_UNTAGGED, is_struct) {
    ${error "Tried to define an untagged struct; only enums can be untagged."}
}}
${if all(tmeta(serde(tag)), is_struct) {
    ${error "Tried to define a tagged struct; only enums can be tagged."}
}}
${if all(tmeta(serde(content)), not(tmeta(serde(tag)))) {
    ${error "Can't use 'content' without 'tag'."}
}}

${define FN_VISIT_UNIT {
    fn visit_unit<E: $crate::m::serde::de::Error>(self) -> $crate::m::Result<Self::Value, E> {
        $crate::m::Ok($vtype)
    }
}}
${define FN_VISIT_NEWTYPE_STRUCT {
    fn visit_newtype_struct<A>(self, deser: A) -> $crate::m::Result<Self::Value, A::Error>
    where A: $crate::m::serde::de::Deserializer<'de__> {
        $( // only expands once
            let v: $ftype = <$ftype as $crate::m::Deserialize>::deserialize(deser)?;
        )
        $crate::m::Ok( $vtype(v) )
    }
}}
${define FN_VISIT_SEQ {
    fn visit_seq<A>(self, mut seq: A) -> $crate::m::Result<Self::Value, A::Error>
    where A: $crate::m::serde::de::SeqAccess<'de__> {
        let mut n = 0;
        $(
            ${when not(F_SKIP)}
            let $< fval_ $fname > = seq.next_element::<$ftype>()?
                // XXXX correct the error message.
                .ok_or_else(|| $crate::m::serde::de::Error::invalid_length(n, &"struct with right number elts"))?;
            n += 1; // XXXX This is a kludge.  We could use NUMBER__FieldId___$fname, but that's for something different.
        )
        $crate::m::Ok($vtype {
            $(
                ${if F_SKIP {
                    $fname: $F_DEFAULT,
                } else {
                    $fname: $<fval_ $fname>,
                }}
            )
        })
    }
}}

${define FN_VISIT_MAP {
    fn visit_map<A>(self, mut map: A) -> $crate::m::Result<Self::Value, A::Error>
    where A: $crate::m::serde::de::MapAccess<'de__> {
        #[derive($crate::derive_deftly::Deftly)]
        #[derive_deftly($crate::Ident)]
        #[allow(non_camel_case_types)]
        enum FieldIdent_ {
            $( ${when not(F_SKIP)}
               $F_NAME_ATTR
               $fname,
            )
            __ignored__,
        }
        impl FieldIdent_ {
            fn expectation(formatter: &mut $crate::m::fmt::Formatter) -> $crate::m::fmt::Result {
                ${if is_enum {
                    $crate::m::write!(
                        formatter,
                        "field identifier for {}::{}",
                            $crate::m::stringify!($tname),
                            $crate::m::stringify!($vname)
                    )
                } else {
                    $crate::m::write!(
                        formatter,
                        "field identifier for {}",
                            $crate::m::stringify!($tname),
                    )
                }}
            }
        }
        const FIELDS: &[&str] = &[
            $( ${when not(F_SKIP)} $crate::m::stringify!($fname), )
        ];

        $(
            ${when not(F_SKIP)}
            let mut $<fval_ $fname> : $crate::m::Option<$ftype> = $crate::m::None;
        )
        while let Some(key) = map.next_key::<FieldIdent_>()? {
            match key {
                $(
                    ${when not(F_SKIP)}
                    FieldIdent_::$fname => {
                        if $<fval_ $fname>.is_some() {
                            return $crate::m::Err($crate::m::serde::de::Error::duplicate_field(stringify!($fname)));
                        }
                        $<fval_$ fname> = $crate::m::Some(map.next_value::<$ftype>()?);
                    }
                )
                FieldIdent_::__ignored__ => {
                    let _ignored = map.next_value::<$crate::m::serde::de::IgnoredAny>()?;
                }
            }
        }
        $crate::m::Ok($vtype {
            $(
                ${if F_SKIP {
                    $fname: $F_DEFAULT,
                } else {
                    $fname: $<fval_ $fname>
                        .ok_or_else(|| $crate::m::serde::de::Error::missing_field(stringify!($fname)))?,
                }}
            )
        })
    }
}}

${define DECL_VARIANT_VISITOR {
    struct VarVisitor<'de__, $tdefgens> where $twheres {
        marker: $crate::m::PhantomData<$ttype>,
        lifetime: $crate::m::PhantomData<&'de__ ()>,
    }
    impl<'de__, $tgens> $crate::m::serde::de::Visitor<'de__> for VarVisitor<'de__, $tgnames> where $twheres {
        type Value = $ttype;
        fn expecting(&self, formatter: &mut $crate::m::fmt::Formatter) -> $crate::m::fmt::Result {
            $crate::m::write!(formatter, "{} variant {}::{}",
                ${if v_is_tuple { "tuple" } else {"struct"}},
                $crate::m::stringify!($tname),
                $crate::m::stringify!($vname))
        }
        ${FN_VISIT_SEQ}
        ${if AS_MAP { $FN_VISIT_MAP }}
    }
}}

${define DECODE_VARIANT {
    use $crate::m::serde::de::VariantAccess;
    ${if v_is_unit {
        variant.unit_variant()?;
        $crate::m::Ok($vtype)
    } else if V_IS_NEWTYPE {
        $( // only repeats once.
        variant.newtype_variant().map($vtype)
        )
    } else if v_is_tuple {
        $DECL_VARIANT_VISITOR
        variant.tuple_variant(
            $N_FIELDS,
            VarVisitor {
                marker: $crate::m::PhantomData,
                lifetime: $crate::m::PhantomData,
            }
        )
     } else {
        $DECL_VARIANT_VISITOR
        variant.struct_variant(
            &[ $( ${when not(F_SKIP)} $crate::m::stringify!($fname) ,) ],
            VarVisitor {
                marker: $crate::m::PhantomData,
                lifetime: $crate::m::PhantomData,
            }
        )
    }}
}}

// TODO: share more with DECODE_VARIANT?
${define DESER_UNTAGGED {
    // See "note on de::private"
    use $crate::m::serde::de::Deserializer as _;
    let content = <$crate::de_private::Content as $crate::m::Deserialize>::deserialize(deserializer)?;
    let deserializer = $crate::de_private::ContentRefDeserializer::<D::Error>::new(&content);
    $(
        ${when not(V_SKIP)}
        if let $crate::m::Ok(val_) = {
            ${if v_is_unit {
                deserializer.deserialize_any($crate::de_private::UntaggedUnitVisitor::new(
                    $crate::m::stringify!($tname),
                    $crate::m::stringify!($vname)
                )).map(|()| $vtype)
            } else if V_IS_NEWTYPE {
                ${for fields { // only repeats once
                <$ftype as $crate::m::Deserialize>::deserialize(deserializer)
                    .map($vtype)
                }}
            } else if v_is_tuple {
                $DECL_VARIANT_VISITOR
                deserializer.deserialize_tuple(
                    $N_FIELDS,
                    VarVisitor {
                        marker: $crate::m::PhantomData,
                        lifetime: $crate::m::PhantomData,
                    }
                )
            } else { // struct case.
                $DECL_VARIANT_VISITOR
                // ??? Serde emits this but doesn't use it.
                const FIELDS: &[&str] = &[
                    $( ${when not(F_SKIP)} $crate::m::stringify!($fname), )
                ];
                // ??? In this case, serde emits deserialize_any.
                // I think that's wrong?
                deserializer.deserialize_struct(
                    $crate::m::stringify!($tname),
                    FIELDS,
                    VarVisitor {
                        marker: $crate::m::PhantomData,
                        lifetime: $crate::m::PhantomData,
                    }
                )
            }}

        } {
            return $crate::m::Ok(val_);
        }
    )

    $crate::m::Err($crate::m::serde::de::Error::custom(concat!(
        "data did not match any variant of untagged enum ",
        $crate::m::stringify!($tname),
    )))
}}

${define DESER_INTERNALLY_TAGGED {
    use $crate::m::serde::de::Deserializer as _;
    let (tag, content) = deserializer.deserialize_any(
        $crate::de_private::TaggedContentVisitor::< VariantId_>::new(
            ${tmeta(serde(tag)) as str},
            $crate::m::concat!("internally tagged enum ", $crate::m::stringify!($tname))),
    )?;
    let deserializer = $crate::de_private::ContentDeserializer::<D::Error>::new(content);
    match tag {
        $(
            ${when not(V_SKIP)}
            VariantId_::$vname => {
                ${if v_is_unit {
                    let () = deserializer.deserialize_any(
                        $crate::de_private::InternallyTaggedUnitVisitor::new(
                            $crate::m::stringify!($tname),
                            $crate::m::stringify!($vname),
                        )
                    )?;
                    Ok($vtype)
                } else if V_IS_NEWTYPE {
                    ${for fields { // only expands once
                    <$ftype as $crate::m::Deserialize>::deserialize(deserializer)
                        .map($vtype)
                    }}
                } else {
                    $DECL_VARIANT_VISITOR
                    deserializer.deserialize_any(
                        VarVisitor {
                            marker: $crate::m::PhantomData,
                            lifetime: $crate::m::PhantomData,
                        }
                    )
                }}
            }
        )
    }
}}

// TODO: Share more with DECODE_UNTAGGED,
${define DECODE_ADJACENTLY_TAGGED_VARIANT {
    ${if not(v_is_unit) {
        let content_ = content_.ok_or_else(|| $crate::m::serde::de::Error::missing_field(${tmeta(serde(content)) as str}))?;
        let deserializer = $crate::de_private::ContentRefDeserializer::<A::Error>::new(&content_);
    }}

    ${if v_is_unit {
        // I believe nothing further is needed; any content is okay?
        // TODO: check that ^!
        Ok($vtype)
    } else if V_IS_NEWTYPE {
        ${for fields { // only once
        <$ftype as $crate::m::Deserialize>::deserialize(deserializer)
        }}
        .map($vtype)
    } else if v_is_tuple {
        $DECL_VARIANT_VISITOR
        deserializer.deserialize_tuple(
            $N_FIELDS,
            VarVisitor {
                marker: $crate::m::PhantomData,
                lifetime: $crate::m::PhantomData,
            }
        )
    } else { // struct case
        $DECL_VARIANT_VISITOR
           // ??? Serde emits this but doesn't use it.
           const FIELDS: &[&str] = &[
            $( ${when not(F_SKIP)} $crate::m::stringify!($fname), )
        ];
        // ??? In this case, serde emits deserialize_any.
        // I think that's wrong?
        deserializer.deserialize_struct(
            $crate::m::stringify!($tname),
            FIELDS,
            VarVisitor {
                marker: $crate::m::PhantomData,
                lifetime: $crate::m::PhantomData,
            }
        )
    } }
}}

${define DESER_ADJACENTLY_TAGGED {
    use $crate::m::serde::de::Deserializer as _;
    // TODO: This is less similar to the serde implementation than the other
    // cases for enums.

    #[derive($crate::derive_deftly::Deftly)]
    #[derive_deftly($crate::Ident)]
    enum OuterFieldId_ {
        #[deftly(name = ${tmeta(serde(tag)) as str })]
        Tag__,
        #[deftly(name = ${tmeta(serde(content)) as str })]
        Content__,
        __ignored__,
    }
    const OUTER_FIELDS: &[&str] = &[
        ${tmeta(serde(tag)) as str},
        ${tmeta(serde(content)) as str},
    ];
    impl OuterFieldId_ {
        fn expectation(formatter: &mut $crate::m::fmt::Formatter) -> $crate::m::fmt::Result {
            write!(formatter, "Field identifier for {}", $crate::m::stringify!($tname))
        }
    }
    struct OuterVisitor__<'de__, $tdefgens> where $twheres {
        marker: $crate::m::PhantomData<$ttype>,
        lifetime: $crate::m::PhantomData<&'de__ ()>,
    }
    impl<'de__, $tgens> $crate::m::serde::de::Visitor<'de__> for OuterVisitor__<'de__, $tgnames> where $twheres
    {
        type Value = $ttype;
        fn expecting(&self, formatter: &mut $crate::m::fmt::Formatter) -> $crate::m::fmt::Result {
            write!(formatter, "Adjacently tagged enum {}", $crate::m::stringify!($tname))
        }
        fn visit_map<A>(self, mut map: A) -> $crate::m::Result<Self::Value, A::Error>
        where A: $crate::m::serde::de::MapAccess<'de__> {
            let mut variant_: $crate::m::Option<VariantId_> = $crate::m::None;
            let mut content_: $crate::m::Option<$crate::de_private::Content<'de__>> = $crate::m::None;
            while let Some(key) = map.next_key::<OuterFieldId_>()? {
                match key {
                    OuterFieldId_::Tag__ => {
                        if variant_.is_some() {
                            return $crate::m::Err($crate::m::serde::de::Error::duplicate_field(${tmeta(serde(tag)) as str}));
                        }
                        variant_ = $crate::m::Some(
                            map.next_value::<VariantId_>()?
                        );
                    }
                    OuterFieldId_::Content__ => {
                        if content_.is_some() {
                            return $crate::m::Err($crate::m::serde::de::Error::duplicate_field(${tmeta(serde(content)) as str}));
                        }
                        content_ = $crate::m::Some(
                             map.next_value::<$crate::de_private::Content>()?
                        );
                    }
                    OuterFieldId_::__ignored__ => {
                        let _ignored = map.next_value::<$crate::m::serde::de::IgnoredAny>()?;
                    }
                }
            }
            let variant_ = variant_.ok_or_else(|| $crate::m::serde::de::Error::missing_field(${tmeta(serde(tag)) as str}))?;
            match variant_ {
                ${for variants {
                    ${when not(V_SKIP)}
                    VariantId_::$vname => {
                        $DECODE_ADJACENTLY_TAGGED_VARIANT
                    }
                    }
                }
            }
        }
    }

    deserializer.deserialize_struct(
        $crate::m::stringify!($tname),
        OUTER_FIELDS,
        OuterVisitor__ {
            marker: $crate::m::PhantomData,
            lifetime: $crate::m::PhantomData,
        }
    )
}}


#[doc(hidden)]
#[allow(non_upper_case_globals, unused_attributes, unused_qualifications)]
    impl<'de__, $tgens> $crate::m::Deserialize<'de__> for $ttype where $twheres {
        fn deserialize<D>(deserializer: D) -> $crate::m::Result<Self, D::Error>
        where D: $crate::m::serde::Deserializer<'de__> {

            ${if HAS_VARIANT_TAG {
                const VARIANTS: &[&str] = &[
                    $(${ when not(V_SKIP)} $crate::m::stringify!($vname), )
                ];
                #[derive($crate::derive_deftly::Deftly)]
                #[derive_deftly($crate::Ident)]
                #[allow(non_camel_case_types)]
                enum VariantId_ { $(
                    ${when not(V_SKIP)}
                    $V_NAME_ATTR
                    $vname,
                ) }
                impl VariantId_ {
                    fn expectation(formatter: &mut $crate::m::fmt::Formatter) -> $crate::m::fmt::Result {
                        $crate::m::write!(
                            formatter,
                            "variant identifier for {}",
                            $crate::m::stringify!($tname)
                        )
                    }
                }
            }}

            ${ if HAS_TOPLEVEL_VISITOR {
            struct Visitor<'de__, $tdefgens> where $twheres {
                marker: $crate::m::PhantomData<$ttype>,
                lifetime: $crate::m::PhantomData<&'de__ ()>,
            }
            impl<'de__, $tgens> $crate::m::serde::de::Visitor<'de__> for Visitor<'de__, $tgnames>  where $twheres {
                type Value = $ttype;
                fn expecting(&self, formatter: &mut $crate::m::fmt::Formatter) -> $crate::m::fmt::Result {
                    ${if is_enum {
                        $crate::m::write!(formatter, "enum {}", stringify!($tname))
                    } else if v_is_tuple {
                        $crate::m::write!(formatter, "tuple struct {}", stringify!($tname))
                    } else {
                        $crate::m::write!(formatter, "struct {}", stringify!($tname))
                    }}
                }
                ${select1 is_struct {
                    ${if v_is_unit { $FN_VISIT_UNIT }}
                    ${if V_IS_NEWTYPE { $FN_VISIT_NEWTYPE_STRUCT }}
                    $FN_VISIT_SEQ
                    ${if AS_MAP { $FN_VISIT_MAP }}
                } else if is_enum {
                    fn visit_enum<A>(self, data: A) -> $crate::m::Result<Self::Value, A::Error>
                    where A: $crate::m::serde::de::EnumAccess<'de__> {
                        match data.variant()? {
                            $(
                                ${when not(V_SKIP)}
                                (VariantId_::$vname, variant) => {
                                   $DECODE_VARIANT
                                }
                            )
                        }
                    }
                }} // endif is_struct
            }
            }} // endif HAS_TOPLEVEL_VISITOR

            ${if ENUM_UNTAGGED {
                $DESER_UNTAGGED
            } else if ENUM_INTERNALLY_TAGGED {
                $DESER_INTERNALLY_TAGGED
            } else if ENUM_ADJACENTLY_TAGGED {
                $DESER_ADJACENTLY_TAGGED
            } else if is_enum {
                deserializer.deserialize_enum(
                    $crate::m::stringify!($tname),
                    VARIANTS,
                    Visitor {
                        marker: $crate::m::PhantomData,
                        lifetime: $crate::m::PhantomData,
                    }
                )
            } else if v_is_unit {
                deserializer.deserialize_unit_struct(
                    $crate::m::stringify!($tname),
                    Visitor {
                        marker: $crate::m::PhantomData,
                        lifetime: $crate::m::PhantomData,
                    }
                )
            } else if V_IS_NEWTYPE {
                deserializer.deserialize_newtype_struct(
                    $crate::m::stringify!($tname),
                    Visitor {
                        marker: $crate::m::PhantomData,
                        lifetime: $crate::m::PhantomData,
                    }
                )
            } else if v_is_tuple {
                deserializer.deserialize_tuple_struct(
                    $crate::m::stringify!($tname),
                    $N_FIELDS,
                    Visitor {  marker: $crate::m::PhantomData,
                        lifetime: $crate::m::PhantomData,}
                )
            } else {
                const FIELDS: &[&str] = &[
                    $( ${when not(F_SKIP)} $crate::m::stringify!($fname), )
                ];
                deserializer.deserialize_struct(
                    $crate::m::stringify!($tname),
                    FIELDS,
                    Visitor {
                        marker: $crate::m::PhantomData,
                        lifetime: $crate::m::PhantomData,
                    }
                )
            }}
        }
    }
}

define_derive_deftly! {
pub Ident for enum =

${define V_NAME {
    ${if vmeta(name) {
        ${vmeta(name) as str}
    } else {
        $crate::m::stringify!($vname)
    }}
}}

$(
    ${when not(approx_equal($vname, __ignored__))}
    const $< NUMBER __ $tname __ $vname > : u64 = $tname::$vname as u64;
    const $< STRING __ $tname __ $vname > : &str = $V_NAME;
    const $< BYTES __ $tname __ $vname > : &[u8] = $V_NAME.as_bytes();
)
${defcond HAS_IGNORED approx_equal(${for variants {
    ${when approx_equal($vname, __ignored__)}
    x
}}, {x})}

    struct $<$tname __IdentVisitor> {}
    impl<'de> $crate::m::serde::de::Visitor<'de> for $<$tname __IdentVisitor> {
        type Value = $ttype;
        fn expecting(&self, formatter: &mut $crate::m::fmt::Formatter) -> $crate::m::fmt::Result {
            $ttype::expectation(formatter)
        }
        fn visit_u64<E: $crate::m::serde::de::Error>(self, val: u64) -> $crate::m::Result<$ttype, E> {
            match val {
                $(
                    $<NUMBER __ $tname __ $vname> => Ok($tname::$vname),
                )
                _ => { ${if HAS_IGNORED {
                    $crate::m::Ok($ttype::__ignored__)
                } else {
                    $crate::m::Err($crate::m::serde::de::Error::invalid_value(
                        $crate::m::serde::de::Unexpected::Unsigned(val),
                        &""
                    ))
                }}}
            }
        }
        fn visit_str<E: $crate::m::serde::de::Error>(self, val: &str) -> $crate::m::Result<$ttype, E> {
            match val {
                $(
                    $<STRING __ $tname __ $vname> => $crate::m::Ok($tname::$vname),
                )
                _ => { ${if HAS_IGNORED {
                    $crate::m::Ok($ttype::__ignored__)
                } else {
                    $crate::m::Err($crate::m::serde::de::Error::unknown_variant(
                        val,
                        VARIANTS,
                    ))
                }}}
            }
        }
        fn visit_bytes<E: $crate::m::serde::de::Error>(self, val: &[u8]) -> $crate::m::Result<$ttype, E> {
            match val {
                $(
                    $<BYTES __ $tname __ $vname> => $crate::m::Ok($tname::$vname),
                )
                _ => { ${if HAS_IGNORED {
                    $crate::m::Ok($ttype::__ignored__)
                } else {
                    let val = $crate::m::String::from_utf8_lossy(val);
                    $crate::m::Err($crate::m::serde::de::Error::unknown_variant(
                        val.as_ref(),
                        VARIANTS,
                    ))
                }}}
            }
        }
    }
    impl<'de> $crate::m::serde::de::Deserialize<'de> for $tname {
        fn deserialize<D>(deserializer: D) -> $crate::m::Result<Self, D::Error>
        where D: $crate::m::serde::Deserializer<'de> {
            deserializer.deserialize_identifier(
                $<$tname __IdentVisitor> {}
            )
        }
    }

}
