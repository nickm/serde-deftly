use derive_deftly::define_derive_deftly;

define_derive_deftly! {
pub Serialize =

${defcond ONE_FIELD approx_equal({${for fields {x}}}, {x}) }
${defcond NO_SKIPS approx_equal(${for fields { ${when F_SKIP} skip }}, {})}
${defcond V_IS_NEWTYPE all(v_is_tuple, ONE_FIELD, NO_SKIPS) }

${define N_FIELDS {( ${for fields { ${when not(F_SKIP)} 1 + }} 0 )}}
${define SUFFIX {${if is_enum { _variant } else { _struct } }}}

${defcond V_SKIP any(vmeta(serde(skip)), vmeta(serde(skip_serializing))) }
${defcond F_SKIP any(fmeta(serde(skip)), fmeta(serde(skip_serializing))) }

${define F_NAME {
    ${if fmeta(serde(rename(serialize))) {
        ${fmeta(serde(rename(serialize))) as str}
    } else if fmeta(serde(rename)) {
        ${fmeta(serde(rename)) as str}
    } else {
        $crate::m::stringify!($fname)
    }}
}}
${defcond F_RENAMED any(fmeta(serde(rename(serialize))), fmeta(serde(rename))) }

${define V_NAME {
    ${if vmeta(serde(rename(serialize))) {
        ${vmeta(serde(rename(serialize))) as str}
    } else if vmeta(serde(rename)) {
        ${vmeta(serde(rename)) as str}
    } else {
        $crate::m::stringify!($vname)
    }}
}}

${define T_NAME {
    ${if tmeta(serde(rename(serialize))) {
        ${tmeta(serde(rename(serialize))) as str}
    } else if tmeta(serde(rename)) {
        ${tmeta(serde(rename)) as str}
    } else {
        $crate::m::stringify!($tname)
    }}
}}

${defcond ENUM_UNTAGGED tmeta(serde(untagged)) }
${defcond ENUM_INTERNALLY_TAGGED all(tmeta(serde(tag)), not(tmeta(serde(content)))) }
${defcond ENUM_ADJACENTLY_TAGGED all(tmeta(serde(tag)), tmeta(serde(content))) }

${defcond AS_TAGGED_VARIANT all(is_enum, not(ENUM_UNTAGGED), not(tmeta(serde(tag)))) }

${define SER_UNIT {${if AS_TAGGED_VARIANT { serialize_unit_variant } else { serialize_unit_struct }}}}
${define SER_NEWTYPE {${if AS_TAGGED_VARIANT { serialize_newtype_variant } else { serialize_newtype_struct }}}}
${define SER_TUPLE {${if AS_TAGGED_VARIANT { serialize_tuple_variant } else { serialize_tuple_struct }}}}
${define SER_TUPLE_TYPE {${if AS_TAGGED_VARIANT { SerializeTupleVariant } else { SerializeTupleStruct }}}}
${define SER_STRUCT {${if AS_TAGGED_VARIANT { serialize_struct_variant } else { serialize_struct }}}}
${define SER_STRUCT_TYPE {${if AS_TAGGED_VARIANT { SerializeStructVariant } else { SerializeStruct }}}}

${define VARIANT_ARGS {${if AS_TAGGED_VARIANT { $<VARIANT_ID_$vname> , $V_NAME, } else {}}}}

${if all(ENUM_UNTAGGED, is_struct) {
    ${error "Tried to define an untagged struct; only enums can be untagged."}
}}
${if all(tmeta(serde(tag)), is_struct) {
    ${error "Tried to define a tagged struct; only enums can be tagged."}
}}
${if all(tmeta(serde(content)), not(tmeta(serde(tag)))) {
    ${error "Can't use 'content' without 'tag'."}
}}

${define IMPL_ADJACENTLY_TAGGED {
    ${if v_is_unit {
        let n_inner__ = 0;
    } else if V_IS_NEWTYPE {
        let n_inner__ = 1;
        let inner__ = ( // only matches once.
            $( $fpatname )
        );
    } else if v_is_tuple {
        let n_inner__ = 1;
        #[derive($crate::derive_deftly::Deftly)]
        #[derive_deftly($crate::Serialize)]
        struct VariantImpl__<'a__, $tdefgens> (
            $( ${when not(F_SKIP)} &'a__ $ftype, )
            #[deftly(serde(skip))]
            $crate::m::PhantomData<$ttype>,
            #[deftly(serde(skip))]
            $crate::m::PhantomData<&'a__ ()>,
        ) where $twheres ;
        let inner__ = VariantImpl__ (
            $( ${when not(F_SKIP)} $fpatname, )
            $crate::m::PhantomData::<$ttype>,
            $crate::m::PhantomData
        );
    } else { // v_is_named
        let n_inner__ = 1;
        #[derive($crate::derive_deftly::Deftly)]
        #[derive_deftly($crate::Serialize)]
        struct VariantImpl__<'a__, $tdefgens> where $twheres {
            $(
                ${when not(F_SKIP)}
                ${if F_RENAMED {  #[deftly(serde(rename = $F_NAME))] }}
                $fname: &'a__ $ftype,
            )
            #[deftly(serde(skip))]
            marker__: $crate::m::PhantomData<$ttype>,
            #[deftly(serde(skip))]
            lifetime__: $crate::m::PhantomData<&'a__ ()>,
        }
        let inner__ = VariantImpl__ {
            $( ${when not(F_SKIP)} $fname: $fpatname, )
            marker__: $crate::m::PhantomData::<$ttype>,
            lifetime__: $crate::m::PhantomData
        };
    }}
    let mut s_outer = serializer.serialize_struct(
        $T_NAME, 1 + n_inner__
    )?;
    $crate::m::serde::ser::SerializeStruct::serialize_field(
        &mut s_outer,
        ${tmeta(serde(tag)) as str},
        $V_NAME
    )?;
    ${ if not(v_is_unit) {
    $crate::m::serde::ser::SerializeStruct::serialize_field(
        &mut s_outer,
        ${tmeta(serde(content)) as str},
        &inner__,
    )?;
    }}
    $crate::m::serde::ser::SerializeStruct::end(s_outer)
}}

impl<$tgens> $crate::m::Serialize for $ttype where $twheres {
    fn serialize<S>(&self, serializer: S) -> $crate::m::Result<S::Ok, S::Error>
    where S: $crate::m::serde::Serializer {
        ${if AS_TAGGED_VARIANT {
           enum VariantId_ { $( ${when not(V_SKIP)} $vname, ) }
           $(
             ${when not(V_SKIP)}
             const $<VARIANT_ID_$vname> : u32 = VariantId_::$vname as u32;
           )
        }}

        match self {
            $(
            ${when not(V_SKIP)}
            $vpat => {
                ${if all(ENUM_INTERNALLY_TAGGED, V_IS_NEWTYPE) {
                    ${ for fields { // only expands once.
                    $crate::ser_private::serialize_tagged_newtype(
                        serializer,
                        $T_NAME,
                        $crate::m::stringify!($vname), // "variant ident"
                        ${tmeta(serde(tag)) as str},
                        $V_NAME, // "variant name" ???????????
                        $fpatname,
                    )
                    }}
                } else if ENUM_INTERNALLY_TAGGED {
                    ${if v_is_tuple {${error "Can't use internally tagged format for tuple variants."}}}
                    let mut s = serializer.serialize_struct(
                        $T_NAME, $N_FIELDS + 1
                    )?;
                    $crate::m::serde::ser::SerializeStruct::serialize_field(
                        &mut s,
                        ${tmeta(serde(tag)) as str},
                        $V_NAME
                    )?;
                    $(
                        ${when not(F_SKIP)}
                        $crate::m::serde::ser::SerializeStruct::serialize_field(
                            &mut s,
                            $F_NAME,
                            $fpatname
                        )?;
                    )
                    $crate::m::serde::ser::SerializeStruct::end(s)
                } else if ENUM_ADJACENTLY_TAGGED {
                    $IMPL_ADJACENTLY_TAGGED
                } else if v_is_unit {
                    serializer.$SER_UNIT(
                        $T_NAME, $VARIANT_ARGS
                    )
                } else if V_IS_NEWTYPE {
                    serializer.$SER_NEWTYPE(
                        $T_NAME,
                        $VARIANT_ARGS
                        $( $fpatname ), // can only repeat once.
                    )
                } else if v_is_tuple {
                    let mut s = serializer.$SER_TUPLE($T_NAME, $VARIANT_ARGS $N_FIELDS)?;
                    $(
                        ${when not(F_SKIP)}
                        $crate::m::serde::ser::$SER_TUPLE_TYPE::serialize_field(
                            &mut s,
                            $fpatname
                        )?;
                    )
                    $crate::m::serde::ser::$SER_TUPLE_TYPE::end(s)
                } else {
                    let mut s = serializer.$SER_STRUCT($T_NAME, $VARIANT_ARGS $N_FIELDS)?;
                    $(
                        ${when not(F_SKIP)}
                        $crate::m::serde::ser::$SER_STRUCT_TYPE::serialize_field(
                            &mut s,
                            $F_NAME,
                            $fpatname
                        )?;
                    )
                    $crate::m::serde::ser::$SER_STRUCT_TYPE::end(s)
                }}
          }
          )
        }
    }
}
}
