use std::fmt::Debug;

pub use derive_deftly;

use derive_deftly::define_derive_deftly;
pub use serde::{self, Deserialize, Serialize};

define_derive_deftly! {
    pub RealSerde =

    #[derive($crate::serde::Serialize, $crate::serde::Deserialize,Clone,Debug,PartialEq,Eq)]
    ${if tmeta(real_serde) { ${tmeta(real_serde) as tokens} }} // XXXX Is there a better way to do this?
    pub $tdefkwd $<$tname RealSerde><$tdefgens>
        ${tdefvariants $(
            ${if vmeta(real_serde) { ${vmeta(real_serde) as tokens} }}
            ${vdefbody $vname $(
               ${if fmeta(real_serde) { ${fmeta(real_serde) as tokens} }}
               ${fdefine $fname} $ftype,
            ) }
        )}

    // XXXX Can this be the right way to do it?  We can't paste onto vtype...
    ${define PAT_CONSTRUCTOR {
        ${if is_enum {
            $<$ttype RealSerde> :: $vname
        } else {
            $<$ttype RealSerde>
        }}
    }}

    impl<$tgens> From<$ttype> for $<$ttype RealSerde> where $twheres {
        fn from(val: $ttype) -> $<$ttype RealSerde> {
            match val {
                $(
                    $vpat => {
                        $PAT_CONSTRUCTOR {
                            $( $fname: $fpatname, )
                        }
                    }
                )
            }
        }
    }

    impl<$tgens> PartialEq<$<$ttype RealSerde>> for $ttype
    where $twheres {
        fn eq(&self, rhs: & $<$ttype RealSerde>) -> bool {
            match self {
                $(
                    $vpat => {
                        $( let $<my_$fname> = $fpatname; )
                        use $<$tname RealSerde> as $tname;
                        if let $vpat = rhs {
                            $(
                                $<my_$fname> == $fpatname &&
                            ) true
                        } else {
                            false
                        }
                    }
                )
            }
     }
    }
}

pub fn check_roundtrip<T, RST>(item: T)
where
    T: Serialize + for<'de> Deserialize<'de> + Clone + Debug + PartialEq<T> + PartialEq<RST>,
    RST: Serialize + for<'de> Deserialize<'de> + Clone + Debug + From<T>,
{
    let theirs = RST::from(item.clone());

    let encoded_ours = serde_json::to_string(&item).unwrap();
    let encoded_theirs = serde_json::to_string(&theirs).unwrap();
    dbg!(&encoded_ours);
    dbg!(&encoded_theirs);

    let decoded_1: T = serde_json::from_str(encoded_ours.as_str()).unwrap();
    let decoded_2: T = serde_json::from_str(encoded_theirs.as_str()).unwrap();
    let decoded_3: RST = serde_json::from_str(encoded_ours.as_str()).unwrap();
    let decoded_4: RST = serde_json::from_str(encoded_theirs.as_str()).unwrap();

    assert_eq!(item, theirs);
    assert_eq!(item, decoded_1);
    assert_eq!(item, decoded_2);
    assert_eq!(item, decoded_3);
    assert_eq!(item, decoded_4);
}

pub fn check_decoding<T, RST>(inp: &str)
where
    T: Serialize + for<'de> Deserialize<'de> + Clone + Debug + PartialEq<T> + PartialEq<RST>,
    RST: Serialize + for<'de> Deserialize<'de> + Clone + Debug + From<T>,
{
    let theirs: RST = serde_json::from_str(inp).unwrap();
    let ours: T = serde_json::from_str(inp).unwrap();

    assert_eq!(ours, theirs);
}

pub fn check_error<T, RST>(inp: &str)
where
    T: Serialize + for<'de> Deserialize<'de> + Clone + Debug + PartialEq<T> + PartialEq<RST>,
    RST: Serialize + for<'de> Deserialize<'de> + Clone + Debug + From<T>,
{
    let _their_err = serde_json::from_str::<RST>(inp).unwrap_err();
    let _our_err = serde_json::from_str::<T>(inp).unwrap_err();

    // XXXX Check error equivalency?
}
