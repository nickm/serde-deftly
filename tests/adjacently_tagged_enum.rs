use derive_deftly::Deftly;

#[derive(Debug, Clone, Deftly, Eq, PartialEq)]
#[derive_deftly(
    serde_deftly::Serialize,
    serde_deftly::Deserialize,
    test_utils::RealSerde
)]
#[deftly(serde(tag = "kind", content = "body"))]
#[deftly(real_serde = r#"#[serde(tag="kind", content="body")]"#)]
enum AllSorts {
    Tuple(u32, u32), // not supported
    Newtype(MyType),
    Unit,
    // XXXX Bug: this has to go last. derive-deftly#63.
    Struct { a: u32, b: u32 },
}

#[derive(Debug, Clone, Deftly, Eq, PartialEq)]
#[derive_deftly(serde_deftly::Serialize, serde_deftly::Deserialize)]
pub struct MyType {
    x: u32,
    y: u32,
}

#[test]
fn r_unit() {
    test_utils::check_roundtrip::<AllSorts, AllSortsRealSerde>(AllSorts::Unit);
}

#[test]
fn r_newtype() {
    test_utils::check_roundtrip::<AllSorts, AllSortsRealSerde>(AllSorts::Newtype(MyType {
        x: 4,
        y: 13,
    }));
}

#[test]
fn r_struct() {
    test_utils::check_roundtrip::<AllSorts, AllSortsRealSerde>(AllSorts::Struct { a: 4, b: 13 });
}
