use derive_deftly::Deftly;

#[derive(Debug, Clone, Deftly, Eq, PartialEq)]
#[derive_deftly(
    serde_deftly::Serialize,
    serde_deftly::Deserialize,
    test_utils::RealSerde
)]
struct Pair<T, const X: usize>
where
    T: std::fmt::Debug
        + Clone
        + Eq
        + PartialEq
        + serde::Serialize
        + for<'de> serde::Deserialize<'de>,
{
    car: T,
    cdr: T,
}

#[derive(Debug, Clone, Deftly, Eq, PartialEq)]
#[derive_deftly(
    serde_deftly::Serialize,
    serde_deftly::Deserialize,
    test_utils::RealSerde
)]
enum Enum1<T, const X: usize>
where
    T: std::fmt::Debug
        + Clone
        + Eq
        + PartialEq
        + serde::Serialize
        + for<'de> serde::Deserialize<'de>,
{
    Tuple(u32, T),
    Newtype(T),
    Unit,
    // XXXX Bug: this has to go last. derive-deftly#63.
    Struct { a: T, b: u32 },
}

#[derive(Debug, Clone, Deftly, Eq, PartialEq)]
#[derive_deftly(
    serde_deftly::Serialize,
    serde_deftly::Deserialize,
    test_utils::RealSerde
)]
#[deftly(serde(untagged))]
enum Enum2<T, const X: usize>
where
    T: std::fmt::Debug + Clone + Eq + PartialEq + serde::Serialize + for<'x> serde::Deserialize<'x>,
{
    Tuple(u32, T),
    Newtype(T),
    Unit,
    // XXXX Bug: this has to go last. derive-deftly#63.
    Struct { a: T, b: u32 },
}

#[derive(Debug, Clone, Deftly, Eq, PartialEq)]
#[derive_deftly(
    serde_deftly::Serialize,
    serde_deftly::Deserialize,
    test_utils::RealSerde
)]
#[deftly(serde(tag = "xxx"))]
enum Enum3<T, const X: usize>
where
    T: std::fmt::Debug + Clone + Eq + PartialEq + serde::Serialize + for<'x> serde::Deserialize<'x>,
{
    //Tuple(u32, T),
    Newtype(T),
    Unit,
    // XXXX Bug: this has to go last. derive-deftly#63.
    Struct { a: T, b: u32 },
}

#[derive(Debug, Clone, Deftly, Eq, PartialEq)]
#[derive_deftly(
    serde_deftly::Serialize,
    serde_deftly::Deserialize,
    test_utils::RealSerde
)]
#[deftly(serde(tag = "a", content = "b"))]
enum Enum4<T, const X: usize>
where
    T: std::fmt::Debug + Clone + Eq + PartialEq + serde::Serialize + for<'x> serde::Deserialize<'x>,
{
    Tuple(u32, T),
    Newtype(T),
    Unit,
    // XXXX Bug: this has to go last. derive-deftly#63.
    Struct { a: T, b: u32 },
}

// TODO: Test that these actually _work_; not just that they compile.
