use derive_deftly::Deftly;

#[derive(Debug, Clone, Deftly, Eq, PartialEq)]
#[derive_deftly(
    serde_deftly::Serialize,
    serde_deftly::Deserialize,
    test_utils::RealSerde
)]
struct Rgb {
    r: u8,
    g: u8,
    b: u8,
}

#[test]
fn check_rgb() {
    let rgb = Rgb { r: 1, g: 2, b: 3 };

    test_utils::check_roundtrip::<Rgb, RgbRealSerde>(rgb);

    test_utils::check_decoding::<Rgb, RgbRealSerde>(
        r#"
        [1, 2, 3]
        "#,
    );

    test_utils::check_decoding::<Rgb, RgbRealSerde>(
        r#"
        { "r": 1, "g": 2, "b" : 3, "ignored": 1337 }
        "#,
    );
}

#[derive(Debug, Clone, Deftly, Eq, PartialEq)]
#[derive_deftly(
    serde_deftly::Serialize,
    serde_deftly::Deserialize,
    test_utils::RealSerde
)]

struct Newtype(String);

#[test]
fn check_newtype() {
    let v = Newtype("hello".into());

    test_utils::check_roundtrip::<Newtype, NewtypeRealSerde>(v);

    test_utils::check_decoding::<Newtype, NewtypeRealSerde>(
        r#"
        "hello"
        "#,
    );
}

#[derive(Debug, Clone, Deftly, Eq, PartialEq)]
#[derive_deftly(
    serde_deftly::Serialize,
    serde_deftly::Deserialize,
    test_utils::RealSerde
)]
struct Tuple(bool, String, u32);

#[test]
fn check_tuple() {
    let v = Tuple(true, "hello".into(), 77);

    test_utils::check_roundtrip::<Tuple, TupleRealSerde>(v);

    test_utils::check_decoding::<Tuple, TupleRealSerde>(
        r#"
        [ false, "a young man stands in his bedroom", 413 ]
        "#,
    );
}

#[derive(Debug, Clone, Deftly, Eq, PartialEq)]
#[derive_deftly(
    serde_deftly::Serialize,
    serde_deftly::Deserialize,
    test_utils::RealSerde
)]
struct Unit;
#[test]
fn check_unit() {
    let v = Unit;

    test_utils::check_roundtrip::<Unit, UnitRealSerde>(v);

    test_utils::check_decoding::<Unit, UnitRealSerde>("null");
    test_utils::check_error::<Unit, UnitRealSerde>("[]");
    test_utils::check_error::<Unit, UnitRealSerde>("{}");
}
